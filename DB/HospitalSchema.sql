CREATE TABLE address 
(
	id VARCHAR(10) NOT NULL PRIMARY KEY,
	street_name VARCHAR(25) NOT NULL,
	street_num VARCHAR(25),
	city VARCHAR(25) NOT NULL,
	country VARCHAR(25) NOT NULL,
	active BIT NOT NULL
)

CREATE TABLE users
(
	jmbg VARCHAR(13) NOT NULL PRIMARY KEY,
	uname VARCHAR(25) NOT NULL,
	lastName VARCHAR(25) NOT NULL,
	userName VARCHAR(25) NOT NULL,
	upassword VARCHAR(25) NOT NULL,
	uemail VARCHAR(25) NOT NULL,
	address_id VARCHAR(25) NOT NULL,
	gender VARCHAR(1) NOT NULL,
	usertype VARCHAR(25) NOT NULL,
	active BIT NOT NULL
)

CREATE TABLE admins
(
	jmbg VARCHAR(13) NOT NULL PRIMARY KEY,
)

CREATE TABLE appointments 
(
	id VARCHAR(10) NOT NULL PRIMARY KEY,
	doctort_jmbg VARCHAR(13),
	patient_jmbg VARCHAR(13),
	status VARCHAR(13),
	a_date DATE,
	hcc_id VARCHAR(10),
	active BIT NOT NULL
)

CREATE TABLE healthCareCentre
(
	id VARCHAR(10) NOT NULL PRIMARY KEY,
	hcc_name VARCHAR(35) NOT NULL,
	address_id VARCHAR(10) NOT NULL,
	active BIT NOT NULL
)

CREATE TABLE therapies
(
	id VARCHAR(10) NOT NULL PRIMARY KEY,
	doctor_jmbg VARCHAR(13),
	patient_jmbg VARCHAR(13),
	t_description VARCHAR(50),	
	active BIT NOT NULL
)

CREATE TABLE doctors
(
	jmbg VARCHAR(13) NOT NULL PRIMARY KEY,
	hcc_id VARCHAR(10) NOT NULL
)