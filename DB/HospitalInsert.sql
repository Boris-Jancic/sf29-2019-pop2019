INSERT INTO address VALUES
	('1', 'Despota Stefana', '7', 'Novi Sad', 'Srbija', 'true'),
	('2', 'Lukijana Musickog', '75', 'Smederevo', 'Srbija', 'true'),
	('3', 'Radinac BB', '', 'Smederevo', 'Srbija', 'true'),
	('4', 'Irene Zekovic', '7', 'Smederevo', 'Srbija', 'true'),
	('5', 'Crvene Armije', '47', 'Smederevo', 'Srbija', 'true'),
	('6', 'Futoska', '9', 'Novi Sad', 'Srbija', 'true'),
	('7', 'Crnotravska', '17', 'Beograd', 'Srbija', 'true')

INSERT INTO users VALUES
	('2508000760014', 'Boris', 'Jancic', 'boris', '1234', 'boris@gmail.com', '2', 'M', 'ADMINISTRATOR', 'true'),
	('0504010760555', 'Andjela', 'Joksimovic', 'andj', '1234', 'andjela@gmail.com', '3', 'F', 'LEKAR', 'true'),
	('1106000760420', 'Tadija', 'Marjanovic', 'tadija', '1234', 'tadija@gmail.com', '5', 'M', 'LEKAR', 'true'),
	('1206990760874', 'Katarina', 'Ljusic', 'kaca', '1234', 'kaca@gmail.com', '4', 'F', 'PACIJENT', 'true'),
	('2803000760114', 'Lazar', 'Ristic', 'lazar', '1234', 'lazar@gmail.com', '5', 'M', 'PACIJENT', 'true'),
	('1208000969420', 'Tijana', 'Mandzic', 'tijana', '1234', 'tijana@gmail.com', '6', 'F', 'PACIJENT', 'true')


INSERT INTO doctors VALUES
	('0504010760555', '1'),
	('1106000760420', '2')

INSERT INTO admins VALUES
	('2508000760014')

INSERT INTO appointments VALUES
	('1', '0504010760555', '2803000760114', 'ZAKAZAN', '2020-02-20', '1', 'true'),
	('2', '1106000760420', '1206990760874', 'SLOBODAN', '2020-02-23', '1','true'),
	('3', '0504010760555', '2803000760114', 'ZAKAZAN', '2020-02-24', '2', 'true'),
	('4', '0504010760555', '1206990760874', 'SLOBODAN', '2020-02-25', '1', 'true'),
	('5', '1106000760420', '2803000760114', 'ZAKAZAN', '2020-02-26', '2', 'true')



INSERT INTO healthCareCentre VALUES
	('1', 'Sveti Luka', '4', 'true'),
	('2', 'Dom Zdravlja Vojvodina', '6', 'true')

INSERT INTO therapies VALUES
	('1', '0504010760555', '1206990760874', 'Paracatamol 2x na dan', 'true'),
	('2', '0504010760555', '1206990760874', 'Ospamax 4x na dan', 'true'),
	('3', '0504010760555', '1206990760874', 'Paracatamol 2x na dan', 'true'),
	('4', '0504010760555', '1206990760874', 'Ospamax 4x na dan', 'true'),
	('5', '0504010760555', '2803000760114', 'Paracatamol 2x na dan', 'true'),
	('6', '1106000760420', '2803000760114', 'Ospamax 4x na dan', 'true'),
	('7', '1106000760420', '1206990760874', 'Paracatamol 2x na dan', 'true'),
	('8', '1106000760420', '2803000760114', 'Ospamax 4x na dan', 'true')