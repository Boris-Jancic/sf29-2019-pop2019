﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{   
    [Serializable]
    public class Appointment
    {
        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _doctorJMBG;

        public string DoctorJMBG
        {
            get { return _doctorJMBG; }
            set { _doctorJMBG = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private string _patientJMBG;

        public string PatientJMBG
        {
            get { return _patientJMBG; }
            set { _patientJMBG = value; }
        }

        private EStatus _status;

        public EStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        private string _healthCareCentreID;

        public string HealthCareCentreID
        {
            get { return _healthCareCentreID; }
            set { _healthCareCentreID = value; }
        }


        public Appointment()
        {

        }

    }
}
