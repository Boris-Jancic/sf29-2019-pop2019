﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{
    public class Patient
    {

        private string _userJMBG;

        public string UserJMBG
        {
            get { return _userJMBG; }
            set { _userJMBG = value; }
        }

        private List<string> _patientAppointments;

        public List<string> PatientAppointments
        {
            get { return _patientAppointments; }
            set { _patientAppointments = value; }
        }

        private List<string> _therapies;

        public List<string> Therapies
        {
            get { return _therapies; }
            set { _therapies = value; }
        }

        public Patient()
        {

        }
    }
}
