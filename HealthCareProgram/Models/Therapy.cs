﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{   
    [Serializable]
    public class Therapy
    {
        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _doctorJMBG;

        public string DoctorJMBG
        {
            get { return _doctorJMBG; }
            set { _doctorJMBG = value; }
        }

        private string _patientJMBG;

        public string PatientJMBG
        {
            get { return _patientJMBG; }
            set { _patientJMBG = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public Therapy()
        {

        }


    }
}
