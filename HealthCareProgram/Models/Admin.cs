﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{
    [Serializable]
    public class Admin
    {
        private string _userJMBG;

        public string UserJMBG
        {
            get { return _userJMBG; }
            set { _userJMBG = value; }
        }

        private List<string> _healthCareCentres;

        public List<string> HealthCareCentres
        {
            get { return _healthCareCentres; }
            set { _healthCareCentres = value; }
        }

        private List<string> _users;

        public List<string> Users
        {
            get { return _users; }
            set { _users = value; }
        }

        private List<string> _appointments;

        public List<string> Appointments
        {
            get { return _appointments; }
            set { _appointments = value; }
        }

        private List<string> _therapies;

        public List<string> Therapies
        {
            get { return _therapies; }
            set { _therapies = value; }
        }

        public Admin()
        {

        }

    }
}
