﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{
    public class Address
    {

        private string _id;
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _streetName;

        public string StreetName
        {
            get { return _streetName; }
            set { _streetName = value; }
        }


        private string _streetNumber;

        public string StreetNumber
        {
            get { return _streetNumber; }
            set { _streetNumber = value; }
        }

        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _country;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }



        public override string ToString()
        {
            return ID + "----" + StreetName + "----" + StreetNumber + "----" + City + "----" + Country + "----" + Active; // + ". Moja adresa je " + Adresa.ToString();
        }
    }
}
