﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{
    [Serializable]
    public class HealthCareCentre
    {
        private string _id;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _addressID;   

        public string AddressID
        {
            get { return _addressID; }
            set { _addressID = value; }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public HealthCareCentre()
        {

        }
    }
}
