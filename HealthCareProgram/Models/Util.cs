﻿using HealthCareProgram.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HealthCareProgram.Models
{
    public sealed class Util
    {
        private static readonly Util instance = new Util();
        public static string CONNECTION_STRING = @"Data Source=DELL-PC\SQLEXPRESS;Initial Catalog=tempdb;Integrated Security=True";

        AddressService _addressService = new AddressService();
        AppointmentService _appointmentService = new AppointmentService();
        UserService _userService = new UserService();
        DoctorService _doctorService = new DoctorService();
        HealthCareCentreService _healthCareCentreService = new HealthCareCentreService();
        TherapyService _therapyService = new TherapyService();

        static Util()
        {

        }

        public static Util Instance
        {
            get
            {
                return instance;
            }
        }

        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<Patient> Patients { get; set; }
        public ObservableCollection<Doctor> Doctors { get; set; }
        public ObservableCollection<Admin> Admins { get; set; }
        public ObservableCollection<Address> Addresses { get; set; }
        public ObservableCollection<HealthCareCentre> HealthCareCentres { get; set; }
        public ObservableCollection<Therapy> Therapies { get; set; }
        public ObservableCollection<Appointment> Appointments { get; set; }

        public void Initialize()
        {
            Users = new ObservableCollection<User>();
            Patients = new ObservableCollection<Patient>();
            Doctors = new ObservableCollection<Doctor>();
            Admins = new ObservableCollection<Admin>();
            Addresses = new ObservableCollection<Address>();
            HealthCareCentres = new ObservableCollection<HealthCareCentre>();
            Therapies = new ObservableCollection<Therapy>();
            Appointments = new ObservableCollection<Appointment>();
        }

        public string getRandomID()
        {
            Random rand = new Random();
            int randID = rand.Next(0, 999999);
            return randID.ToString();
        }

        public void readEntities(string fileName)
        {
            if (fileName.Contains("adrese"))
            {
                _addressService.readAddresses();
            }
            else if (fileName.Contains("korisnici"))
            {
                _userService.readUsers();
            }
            else if (fileName.Contains("doktori"))
            {
                _doctorService.readDoctors();
            }
            else if (fileName.Contains("termini"))
            {
                _appointmentService.readAppointments();
            }
            else if (fileName.Contains("bolnice"))
            {
                _healthCareCentreService.readHealthCareCentres();
            }
            else if (fileName.Contains("terapije"))
            {
                _therapyService.readTherapies();
            }
        }

    
        User u = new User();
        Address ad = new Address();
        Appointment ap = new Appointment();
        HealthCareCentre hcc = new HealthCareCentre();
        Therapy t = new Therapy();
        public void addEntity(object obj) 
        { 
            if (obj.GetType() == u.GetType())
            {
                _userService.saveUser(obj);
            }
            else if (obj.GetType() == ad.GetType())
            {
                _addressService.saveAddress(obj);
            }
            else if (obj.GetType() == ap.GetType())
            {
                _appointmentService.saveAppointment(obj);
            }
            else if (obj.GetType() == hcc.GetType())
            {
                _healthCareCentreService.saveHealthCareCentre(obj);
            }
            else if (obj.GetType() == t.GetType())
            {
                _therapyService.saveTherapy(obj);
            }
        }

        public void updateEntity(object obj)
        {
            if (obj.GetType() == u.GetType())
            {
                _userService.updateUser(obj);
            }
            else if (obj.GetType() == ad.GetType())
            {
                _addressService.updateAddress(obj);
            }
            else if (obj.GetType() == ap.GetType())
            {
                _appointmentService.updateAppointment(obj);
            }
            else if (obj.GetType() == hcc.GetType())
            {
                _healthCareCentreService.updateHealthCareCentre(obj);
            }
            else if (obj.GetType() == t.GetType())
            {
                _therapyService.updateTherapy(obj);
            }
        }

        public void deleteEntity(object obj)
        {
            if (obj.GetType() == u.GetType())
            {
                _userService.deleteUser(obj);
            }
            else if (obj.GetType() == ad.GetType())
            {
                _addressService.deleteAddress(obj);
            }
            else if (obj.GetType() == ap.GetType())
            {
                _appointmentService.deleteAppointment(obj);
            }
            else if (obj.GetType() == hcc.GetType())
            {
                _healthCareCentreService.deleteHealthCareCentre(obj);
            }
            else if (obj.GetType() == t.GetType())
            {
                _therapyService.deleteTherapy(obj);
            }
        }

        public void restoreEntity(object obj)
        {
            if (obj.GetType() == u.GetType())
            {
                _userService.restoreUser(obj);
            }
            else if (obj.GetType() == ad.GetType())
            {
                _addressService.restoreAddress(obj);
            }
            else if (obj.GetType() == ap.GetType())
            {
                _appointmentService.restoreAppointment(obj);
            }
            else if (obj.GetType() == hcc.GetType())
            {
                _healthCareCentreService.restoreHealthCareCentre(obj);
            }
            else if (obj.GetType() == t.GetType())
            {
                _therapyService.restoreTherapy(obj);
            }
        }

        public User checkLogin(string jmbg, string password)
        {
            foreach (User user in Users)
            {
                if (user.JMBG.Equals(jmbg) && user.Password.Equals(password) && user.Active)
                {
                    return user;
                }
            }
            return null;
        }

        public ObservableCollection<User> getDoctors()
        {
            ObservableCollection<User> doctors = new ObservableCollection<User>();

            foreach (User doctor in Users)
            {
                if (doctor.UserType == EType.LEKAR && doctor.Active)
                {
                    doctors.Add(doctor);
                }
            }

            return doctors;
        }


        public ObservableCollection<Therapy> findTherapies(string jmbg)
        {
            ObservableCollection<Therapy> therapies = new ObservableCollection<Therapy>();

            foreach (Therapy therapy in Therapies)
            {
                if (therapy.PatientJMBG.Equals(jmbg) && therapy.Active)
                {
                    therapies.Add(therapy);
                }
            }

            return therapies;
        }

        public bool ifDoctorActive(string jmbg)
        {
            foreach (User user in Users)
            {
                if (user.JMBG.Equals(jmbg) && user.Active)
                {
                    return true;
                }
            }
            return false;
        }

        public Address findAddress(string id)
        {
            foreach (Address address in Addresses)
            {
                if (address.ID.Equals(id) && address.Active)
                {
                    return address;
                }
            }
            return null;
        }

        public ObservableCollection<User> findRegisteredUsers()
        {
            ObservableCollection<User> users = new ObservableCollection<User>();

            foreach (User user in Users)
            {
                if ((user.UserType == EType.PACIJENT || user.UserType == EType.LEKAR) && user.Active)
                {
                    users.Add(user);
                }
            }

            return users;
        }

        public ObservableCollection<Appointment> findPatientAppointments(string JMBG)
        {
            ObservableCollection<Appointment> appointments = new ObservableCollection<Appointment>();

            foreach (Appointment appointment in Appointments)
            {
                if (appointment.PatientJMBG.Equals(JMBG) && appointment.Active)
                {
                    appointments.Add(appointment);
                }
            }

            return appointments;
        }

        public ObservableCollection<Appointment> findDoctorAppointments(string JMBG)
        {
            ObservableCollection<Appointment> appointments = new ObservableCollection<Appointment>();

            foreach (Appointment appointment in Appointments)
            {
                if (appointment.DoctorJMBG.Equals(JMBG) && appointment.Active)
                {
                    appointments.Add(appointment);
                }
            }

            return appointments;
        }

        public ObservableCollection<Appointment> findAvailableAppointments()
        {
            ObservableCollection<Appointment> appointments = new ObservableCollection<Appointment>();

            foreach (Appointment appointment in Appointments)
            {
                if (appointment.Status == EStatus.SLOBODAN && appointment.Active)
                {
                    appointments.Add(appointment);
                }
            }

            return appointments;
        }

        public ObservableCollection<User> activeUsers()
        {
            ObservableCollection<User> users = new ObservableCollection<User>();

            foreach (User user in Users)
            {
                if (user.Active)
                {
                    users.Add(user);
                }
            }

            return users;
        }

        public ObservableCollection<Address> activeAddresses()
        {
            ObservableCollection<Address> addresses = new ObservableCollection<Address>();

            foreach (Address address in Addresses)
            {
                if (address.Active)
                {
                    addresses.Add(address);
                }
            }

            return addresses;
        }

        public ObservableCollection<Appointment> activeAppointments()
        {
            ObservableCollection<Appointment> appointments = new ObservableCollection<Appointment>();

            foreach (Appointment appointment in Appointments)
            {
                if (appointment.Active)
                {
                    appointments.Add(appointment);
                }
            }

            return appointments;
        }

        public ObservableCollection<HealthCareCentre> activeHealthCareCentres()
        {
            ObservableCollection<HealthCareCentre> healthCareCentres = new ObservableCollection<HealthCareCentre>();

            foreach (HealthCareCentre healthCareCentre in HealthCareCentres)
            {
                if (healthCareCentre.Active)
                {
                    healthCareCentres.Add(healthCareCentre);
                }
            }

            return healthCareCentres;
        }

        public ObservableCollection<Therapy> activeTherapies()
        {
            ObservableCollection<Therapy> therapies = new ObservableCollection<Therapy>();
             
            foreach (Therapy therapy in Therapies)
            {
                if (therapy.Active)
                {
                    therapies.Add(therapy);
                }
            }

            return therapies;
        }

        public ObservableCollection<Therapy> getDoctorTherapies(String jmbg)
        {
            ObservableCollection<Therapy> therapies = new ObservableCollection<Therapy>();

            foreach (Therapy therapy in Therapies)
            {
                if (therapy.DoctorJMBG.Equals(jmbg) && therapy.Active)
                {
                    therapies.Add(therapy);
                }
            }

            return therapies;
        }
    }
}