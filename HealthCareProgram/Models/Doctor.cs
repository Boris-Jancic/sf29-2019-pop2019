﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Models
{   
    [Serializable]
    public class Doctor
    {
        private string _userJMBG; 

        public string UserJMBG
        {
            get { return _userJMBG; }
            set { _userJMBG = value; }
        }

        private string _healthCareCentreID;

        public string HealthCareCentreID
        {
            get { return _healthCareCentreID; }
            set { _healthCareCentreID = value; }
        }

        public Doctor()
        {

        }
    }
}
