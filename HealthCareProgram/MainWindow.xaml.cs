﻿using HealthCareProgram.Models;
using HealthCareProgram.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HealthCareProgram
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        User loggedUser = new User();

        public MainWindow(User user)
        {
            Util.Instance.readEntities("termini");
            InitializeComponent();

            loggedUser = user;

            if (user == null)
            {
                lblName.Content = "Neregistrovani korisnik";
                lblUserType.Content = "Neregistrovani korisnik";

                btnDoctors.IsEnabled = false;
                btnUsers.IsEnabled = false;
                btnEditInfo.IsEnabled = false;

                btnAppointments.IsEnabled = false;
                btnTherapies.IsEnabled = false;
            }
            else if (user.UserType == EType.PACIJENT)
            {
                lblName.Content = user.Name + " " + user.LastName;
                lblUserType.Content = user.UserType.ToString();

                btnAvailableAppointments.IsEnabled = true;
                btnUsers.IsEnabled = false;
            }
            else if (user.UserType == EType.LEKAR || user.UserType == EType.ADMINISTRATOR)
            {
                lblName.Content = user.Name + " " + user.LastName;
                lblUserType.Content = user.UserType.ToString();
            }
        }

        private void AllUsers_Click(object sender, RoutedEventArgs e)
        {
            AllUsersWindow window = new AllUsersWindow(loggedUser);

            window.Show();
        }

        private void Patient_Click(object sender, RoutedEventArgs e)
        {
            PatientWindow window = new PatientWindow();

            window.Show();
        }

        private void Doctors_Click(object sender, RoutedEventArgs e)
        {

            DoctorWindow window = new DoctorWindow();

            window.Show();
        }

        private void Appointment_Click(object sender, RoutedEventArgs e)
        {
            AppointmentWindow window = new AppointmentWindow(loggedUser);

            window.Show();
        }

        private void Therapy_Click(object sender, RoutedEventArgs e)
        {
            TherapyWindow window = new TherapyWindow(loggedUser);

            window.Show();
        }

        private void HealthCareCentre_Click(object sender, RoutedEventArgs e)
        {
            HealthCareCentreWindow window = new HealthCareCentreWindow(loggedUser);

            window.Show();
        }

        private void Address_Click(object sender, RoutedEventArgs e)
        {
            AddressessWindow window = new AddressessWindow(loggedUser);

            window.Show();
        }

        private void EditInfo_Click(object sender, RoutedEventArgs e)
        {
            AddEditUser addEditUser = new AddEditUser(loggedUser, EAddEdit.EDIT);
            addEditUser.CmbType.IsEnabled = false;
            addEditUser.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow();

            loginWindow.Show();
            this.Close();
        }

        private void btnAvailableAppointments_Click(object sender, RoutedEventArgs e)
        {
            AvailableAppointmentsWindow window = new AvailableAppointmentsWindow(loggedUser);

            window.Show();
        }
    }
}
