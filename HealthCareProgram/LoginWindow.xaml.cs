﻿using HealthCareProgram.Models;
using HealthCareProgram.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            Util.Instance.Initialize();
            Util.Instance.readEntities("korisnici");
        }

        private void Login_click(object sender, RoutedEventArgs e)
        {
            User user = Util.Instance.checkLogin(TxtJMBG.Text, PfPassword.Password);

            if (user != null)
            {
                MainWindow mainWindow = new MainWindow(user);
                mainWindow.Show();

                this.Close();
            } 
            else
            {
                LblMistake.Content = "Pogresan unos JMBG-a ili sifre";
            }
        }

        private void NotRegistered_click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(null);
            mainWindow.Show();

            this.Close();
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            AddPatient addPatient = new AddPatient();

            addPatient.ShowDialog();
        }
    }
}
