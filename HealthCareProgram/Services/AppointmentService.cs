﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Services
{
    public class AppointmentService
    {

        public void readAppointments()
        {
            Util.Instance.Appointments = new ObservableCollection<Appointment>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from appointments";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    string s = reader.GetString(3);
                    Enum.TryParse(s, out EStatus status);

                    Appointment appointmentFromDB = new Appointment
                    {
                        ID = reader.GetString(0),
                        DoctorJMBG = reader.GetString(1),
                        PatientJMBG = reader.GetString(2),
                        Date = reader.GetDateTime(4),
                        HealthCareCentreID = reader.GetString(5),
                        Status = status,
                        Active = reader.GetBoolean(6)
                    };

                    Util.Instance.Appointments.Add(appointmentFromDB);
                }
                reader.Close();
            }
        }

        public void saveAppointment(Object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into appointments (id, doctort_jmbg, patient_jmbg, a_date, hcc_id, status, active)
                                             Values(@id, @doctort_jmbg, @patient_jmbg, @a_date, @hcc_id, @status, @active)";

                command.Parameters.Add(new SqlParameter("id", appointment.ID));
                command.Parameters.Add(new SqlParameter("doctort_jmbg", appointment.DoctorJMBG));
                command.Parameters.Add(new SqlParameter("patient_jmbg", appointment.PatientJMBG));
                command.Parameters.Add(new SqlParameter("a_date", appointment.Date));
                command.Parameters.Add(new SqlParameter("hcc_id", appointment.HealthCareCentreID));
                command.Parameters.Add(new SqlParameter("status", appointment.Status.ToString()));
                command.Parameters.Add(new SqlParameter("active", true));

                command.ExecuteScalar();
            }
        }
        public void updateAppointment(Object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                // TODO: dodaj datum ovde
                command.CommandText = @"update appointments
                                        set id = @id,
                                            doctort_jmbg = @doctort_jmbg,
                                            patient_jmbg = @patient_jmbg,
                                            a_date = @a_date,
                                            hcc_id = @hcc_id,
                                            status = @status
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", appointment.ID));
                command.Parameters.Add(new SqlParameter("doctort_jmbg", appointment.DoctorJMBG));
                command.Parameters.Add(new SqlParameter("patient_jmbg", appointment.PatientJMBG));
                command.Parameters.Add(new SqlParameter("a_date", appointment.Date));
                command.Parameters.Add(new SqlParameter("hcc_id", appointment.HealthCareCentreID));
                command.Parameters.Add(new SqlParameter("status", appointment.Status.ToString()));

                command.ExecuteScalar();
            }
        }
        public void deleteAppointment(Object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update  appointments
                                            set active = 0
                                        where id = @id";

                appointment.Active = false;
                command.Parameters.Add(new SqlParameter("id", appointment.ID));

                command.ExecuteScalar();
            }
        }

        public void restoreAppointment(Object obj)
        {
            Appointment appointment = obj as Appointment;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update  appointments
                                            set active = 1
                                        where id = @id";

                appointment.Active = true;
                command.Parameters.Add(new SqlParameter("id", appointment.ID));

                command.ExecuteScalar();
            }
        }
    }
}
