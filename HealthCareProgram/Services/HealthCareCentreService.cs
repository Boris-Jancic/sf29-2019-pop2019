﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Services
{
    public class HealthCareCentreService
    {
        public void readHealthCareCentres()
        {
            Util.Instance.HealthCareCentres = new ObservableCollection<HealthCareCentre>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from healthCareCentre";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    HealthCareCentre healthCareCentreFromDB = new HealthCareCentre
                    {
                        ID = reader.GetString(0),
                        Name = reader.GetString(1),
                        AddressID = reader.GetString(2),
                        Active = reader.GetBoolean(3)
                    };
                    
                    Util.Instance.HealthCareCentres.Add(healthCareCentreFromDB);

                    Console.WriteLine(healthCareCentreFromDB.ToString());
                }
            }
        }

        public void saveHealthCareCentre(Object obj)
        {
            HealthCareCentre healthCareCentre = obj as HealthCareCentre;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into healthCareCentre (id, hcc_name, address_id, active) 
                                             Values(@id, @hcc_name, @address_id, @active)";

                command.Parameters.Add(new SqlParameter("id", healthCareCentre.ID));
                command.Parameters.Add(new SqlParameter("hcc_name", healthCareCentre.Name));
                command.Parameters.Add(new SqlParameter("address_id", healthCareCentre.AddressID));
                command.Parameters.Add(new SqlParameter("active", true));

                command.ExecuteScalar();
            }
        }
        public void updateHealthCareCentre(Object obj)
        {
            HealthCareCentre healthCareCentre = obj as HealthCareCentre;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update healthCareCentre 
                                        set 
                                            id = @id,
                                            hcc_name = @hcc_name,
                                            address_id = @address_id
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", healthCareCentre.ID));
                command.Parameters.Add(new SqlParameter("hcc_name", healthCareCentre.Name));
                command.Parameters.Add(new SqlParameter("address_id", healthCareCentre.AddressID));

                command.ExecuteScalar();
            }
        }
        public void deleteHealthCareCentre(Object obj)
        {
            HealthCareCentre healthCareCentre = obj as HealthCareCentre;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update healthCareCentre 
                                            set active = 0
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", healthCareCentre.ID));

                command.ExecuteScalar();
            }
        }
        public void restoreHealthCareCentre(Object obj)
        {
            HealthCareCentre healthCareCentre = obj as HealthCareCentre;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update healthCareCentre 
                                            set active = 1
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", healthCareCentre.ID));

                command.ExecuteScalar();
            }
        }
    }
}
