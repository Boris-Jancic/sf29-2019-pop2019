﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Services
{
    public class AddressService
    {
        public void readAddresses()
        {
            Util.Instance.Addresses = new ObservableCollection<Address>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from address";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Address addressFromDB = new Address
                    {
                        ID = reader.GetString(0),
                        StreetName = reader.GetString(1),
                        StreetNumber = reader.GetString(2),
                        City = reader.GetString(3),
                        Country = reader.GetString(4),
                        Active = reader.GetBoolean(5)
                    };

                    Util.Instance.Addresses.Add(addressFromDB);
                    Console.WriteLine(addressFromDB.ToString());
                }

                reader.Close();
            }
        }

        public void saveAddress(Object obj)
        {
            Address address = obj as Address;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into address (id, street_name, street_num, city, country, active)
                                             Values(@id, @street_name, @street_num, @city, @country, @active)";

                command.Parameters.Add(new SqlParameter("id", address.ID));
                command.Parameters.Add(new SqlParameter("street_name", address.StreetName));
                command.Parameters.Add(new SqlParameter("street_num", address.StreetNumber));
                command.Parameters.Add(new SqlParameter("city", address.City));
                command.Parameters.Add(new SqlParameter("country", address.Country));
                command.Parameters.Add(new SqlParameter("active", true));

                command.ExecuteScalar();
            }
        }
        public void updateAddress(Object obj)
        {
            Address address = obj as Address;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update address
                                            set id = @id,
                                            street_name = @street_name,
                                            street_num = @street_num,
                                            city = @city,
                                            country = @country
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", address.ID));
                command.Parameters.Add(new SqlParameter("street_name", address.StreetName));
                command.Parameters.Add(new SqlParameter("street_num", address.StreetNumber));
                command.Parameters.Add(new SqlParameter("city", address.City));
                command.Parameters.Add(new SqlParameter("country", address.Country));

                command.ExecuteScalar();
            }
        }
        public void deleteAddress(Object obj)
        {
            Address address = obj as Address;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update address
                                            set active = 0
                                        where id = @id";
                command.Parameters.Add(new SqlParameter("id", address.ID));

                command.ExecuteScalar();
            }
        }
        public void restoreAddress(Object obj)
        {
            Address address = obj as Address;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update address
                                            set active = 1
                                        where id = @id";
                command.Parameters.Add(new SqlParameter("id", address.ID));

                command.ExecuteScalar();
            }
        }
    }
}
