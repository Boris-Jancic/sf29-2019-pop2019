﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Services
{
    public class DoctorService
    {
        public void readDoctors()
        {
            Util.Instance.Doctors = new ObservableCollection<Doctor>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from doctors";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Doctor doctorFromDB = new Doctor
                    {
                        UserJMBG = reader.GetString(0),
                        HealthCareCentreID = reader.GetString(1),
                    };
                    
                    if (Util.Instance.ifDoctorActive(doctorFromDB.UserJMBG))
                    {
                        Util.Instance.Doctors.Add(doctorFromDB);
                    }
                }

                reader.Close();
            }
        }

        public void saveDoctor(Object obj)
        {
            Doctor doctor = obj as Doctor;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into doctors (jmbg, hcc_id)
                                            values(@jmbg, @hcc_id)";

                command.Parameters.Add(new SqlParameter("jmbg", doctor.UserJMBG));
                command.Parameters.Add(new SqlParameter("hcc_id", doctor.HealthCareCentreID));

                command.ExecuteScalar();
            }
        }

        public void editDoctor(Object obj)
        {
            Doctor doctor = obj as Doctor;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update doctors 
                                            set jmbg = @jmbg,
                                                hcc_id = @hcc_id
                                        where jmbg = @jmbg";

                command.Parameters.Add(new SqlParameter("jmbg", doctor.UserJMBG));
                command.Parameters.Add(new SqlParameter("hcc_id", doctor.HealthCareCentreID));

                command.ExecuteScalar();
            }
        }

    }
}
