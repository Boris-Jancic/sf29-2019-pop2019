﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace HealthCareProgram.Services
{
    public class UserService
    {
        public void readUsers()
        {
            Util.Instance.Users = new ObservableCollection<User>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    string g = reader.GetString(7);
                    Enum.TryParse(g, out EGender gender);

                    string t = reader.GetString(8);
                    Enum.TryParse(t, out EType type);

                    User userFromDB = new User
                    {
                        JMBG = reader.GetString(0),
                        Name = reader.GetString(1),
                        LastName = reader.GetString(2),
                        UserName = reader.GetString(3),
                        Password = reader.GetString(4),
                        Email = reader.GetString(5),
                        Address = reader.GetString(6),
                        Gender = gender,
                        UserType = type,
                        Active = reader.GetBoolean(9)
                    };

                    if (userFromDB.UserType.Equals(EType.PACIJENT))
                    {
                        Patient newPatient = new Patient
                        {
                            UserJMBG = userFromDB.JMBG
                        };
                        Util.Instance.Patients.Add(newPatient);
                    }
                    else if (userFromDB.UserType.Equals(EType.LEKAR))
                    {
                        Doctor newDoctor = new Doctor
                        {
                            UserJMBG = userFromDB.JMBG
                        };
                        Util.Instance.Doctors.Add(newDoctor);
                    }

                    Util.Instance.Users.Add(userFromDB);

                    Console.WriteLine(userFromDB.ToString());
                }

                reader.Close();
            }
        }

        public void saveUser(Object obj)
        {
            User user = obj as User;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into guest.users (uname, lastName, userName, upassword, jmbg, uemail, address_id, gender, usertype, active) 
                                             Values(@uname, @lastName, @userName, @upassword, @jmbg, @uemail, @address, @gender, @usertype, @active)";

                command.Parameters.Add(new SqlParameter("jmbg", user.JMBG));
                command.Parameters.Add(new SqlParameter("uname", user.Name));
                command.Parameters.Add(new SqlParameter("lastName", user.LastName));
                command.Parameters.Add(new SqlParameter("userName", user.UserName));
                command.Parameters.Add(new SqlParameter("upassword", user.Password));
                command.Parameters.Add(new SqlParameter("uemail", user.Email));
                command.Parameters.Add(new SqlParameter("address", user.Address));
                command.Parameters.Add(new SqlParameter("gender", user.Gender.ToString()));
                command.Parameters.Add(new SqlParameter("usertype", user.UserType.ToString()));
                command.Parameters.Add(new SqlParameter("active", true));

                command.ExecuteScalar();
            }
        }

        public void updateUser(Object obj)
        {
            User user = obj as User;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update guest.users
                                        set uname = @uname,
                                            lastName = @lastName,
                                            userName = @userName,
                                            upassword = @upassword,
                                            jmbg = @jmbg,
                                            uemail = @uemail,
                                            address_id = @address_id,
                                            gender = @gender,
                                            usertype = @usertype
                                        where jmbg = @jmbg";

                command.Parameters.Add(new SqlParameter("jmbg", user.JMBG));
                command.Parameters.Add(new SqlParameter("uname", user.Name));
                command.Parameters.Add(new SqlParameter("lastName", user.LastName));
                command.Parameters.Add(new SqlParameter("userName", user.UserName));
                command.Parameters.Add(new SqlParameter("upassword", user.Password));
                command.Parameters.Add(new SqlParameter("uemail", user.Email));
                command.Parameters.Add(new SqlParameter("address_id", user.Address));
                command.Parameters.Add(new SqlParameter("gender", user.Gender.ToString()));
                command.Parameters.Add(new SqlParameter("usertype", user.UserType.ToString()));

                command.ExecuteScalar();
            }
        }
        public void deleteUser(Object obj)
        {
            User user = obj as User;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update users
                                          set active = 0
                                        where userName = @userName";

                user.Active = false;
                command.Parameters.Add(new SqlParameter("userName", user.UserName));
                command.ExecuteScalar();
            }
        }

        public void restoreUser(Object obj)
        {
            User user = obj as User;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update users
                                          set active = 1
                                        where userName = @userName";

                user.Active = true;
                command.Parameters.Add(new SqlParameter("userName", user.UserName));

                command.ExecuteScalar();
            }
        }
    }
}
