﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthCareProgram.Services
{
    public class TherapyService
    {
        public void readTherapies()
        {
            Util.Instance.Therapies = new ObservableCollection<Therapy>();

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from therapies";

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    Therapy therapyFromDB = new Therapy
                    {
                        ID = reader.GetString(0),
                        DoctorJMBG = reader.GetString(1),
                        PatientJMBG = reader.GetString(2),
                        Description = reader.GetString(3),
                        Active = reader.GetBoolean(4)
                    };

                    Util.Instance.Therapies.Add(therapyFromDB);

                    Console.WriteLine(therapyFromDB.ToString());
                }

                reader.Close();
            }
        }

        public void saveTherapy(Object obj)
        {
            Therapy therapy = obj as Therapy;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into therapies (id, doctor_jmbg, patient_jmbg, t_description, active) 
                                             Values(@id, @doctor_jmbg, @patient_jmbg, @t_description, @active)";

                command.Parameters.Add(new SqlParameter("id", therapy.ID));
                command.Parameters.Add(new SqlParameter("doctor_jmbg", therapy.DoctorJMBG));
                command.Parameters.Add(new SqlParameter("patient_jmbg", therapy.PatientJMBG));
                command.Parameters.Add(new SqlParameter("t_description", therapy.Description));
                command.Parameters.Add(new SqlParameter("active", true));

                command.ExecuteScalar();
            }
        }

        public void updateTherapy(Object obj)
        {
            Therapy therapy = obj as Therapy;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update therapies 
                                        set id = @id,
                                            doctor_jmbg = @doctor_jmbg,
                                            patient_jmbg = @patient_jmbg,
                                            t_description = @t_description
                                         where id = @id";

                command.Parameters.Add(new SqlParameter("id", therapy.ID));
                command.Parameters.Add(new SqlParameter("doctor_jmbg", therapy.DoctorJMBG));
                command.Parameters.Add(new SqlParameter("patient_jmbg", therapy.PatientJMBG));
                command.Parameters.Add(new SqlParameter("t_description", therapy.Description));

                command.ExecuteScalar();
            }
        }

        public void deleteTherapy(Object obj)
        {
            Therapy therapy = obj as Therapy;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update therapies 
                                            set active = 0
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", therapy.ID));

                command.ExecuteScalar();
            }
        }
        public void restoreTherapy(Object obj)
        {
            Therapy therapy = obj as Therapy;

            using (SqlConnection conn = new SqlConnection(Util.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update therapies 
                                            set active = 1
                                        where id = @id";

                command.Parameters.Add(new SqlParameter("id", therapy.ID));

                command.ExecuteScalar();
            }
        }
    }
}
