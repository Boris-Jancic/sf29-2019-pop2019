﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for PatientWindow.xaml
    /// </summary>
    public partial class PatientWindow : Window
    {
        ICollectionView view;
        public PatientWindow()
        {
            InitializeComponent();
            CmbType.ItemsSource = Enum.GetValues(typeof(EType)).Cast<EType>();
            UpdateView();
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.findRegisteredUsers());
            DGPatients.ItemsSource = view;
            DGPatients.IsSynchronizedWithCurrentItem = true;

            DGPatients.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view.Filter = patientFilter;
        }

        private void DGPatients_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

        }

        private bool patientFilter(object obj)
        {
            User user = obj as User;

            if (TxtFindName.Text != "")
            {
                return user.Name.Contains(TxtFindName.Text);
            }
            else if (TxtFindLastName.Text != "") 
            {
                return user.LastName.Contains(TxtFindLastName.Text);
            }
            else if (TxtFindEmail.Text != "")
            {
                return user.Email.Contains(TxtFindEmail.Text);
            }
            else if (TxtFindAddress.Text != "")
            {
                return user.Address.Contains(TxtFindAddress.Text);
            }
            else if (CmbType.SelectedItem != null)
            {
                Enum.TryParse(CmbType.SelectedItem.ToString(), out EType type);
                return user.UserType.Equals(type);
            }
            else
                return true;
        }

        private void MIDeletePatient_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Patient selectedPatient = view.CurrentItem as Patient;
                Util.Instance.Patients.Remove(selectedPatient);
            }
        }

        private void TxtFindName_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindLastName_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindEmail_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindAddress_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void CmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            view.Refresh();
        }
    }
}
