﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for AddEditPatient.xaml
    /// </summary>
    public partial class AddPatient : Window
    {

        public AddPatient()
        {
            InitializeComponent();

            CmbGender.ItemsSource = Enum.GetValues(typeof(EGender)).Cast<EGender>();

            CmbGender.SelectedItem = EGender.M;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            string mistakeStr = "";
            bool mistakeBool = true;

            if (TxtName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Ime" + "\n";
                mistakeBool = false;
            }
            if (TxtLastName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Prezime" + "\n";
                mistakeBool = false;
            }
            if (TxtUserName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Korisnicko ime" + "\n";
                mistakeBool = false;
            }
            if (TxtPassword.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Sifru" + "\n";
                mistakeBool = false;
            }
            if (TxtJMBG.Text.Equals("") && TxtJMBG.Text.Length != 13)
            {
                mistakeStr += "- Niste pravilno uneli JMBG" + "\n";
                mistakeBool = false;
            }
            if (TxtEmail.Text.Equals("") || !TxtEmail.Text.Contains("@gmail.com"))
            {
                mistakeStr += "- Niste uneli Email" + "\n";
                mistakeBool = false;
            }
            if (TxtAddress.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli ID Adrese" + "\n";
                mistakeBool = false;
            }
            if (CmbGender.SelectedItem == null)
            {
                mistakeStr += "- Niste odabrali pol" + "\n";
                mistakeBool = false;
            }

            if (mistakeBool)
            {
                string genderValue = CmbGender.SelectedItem.ToString();
                Enum.TryParse(genderValue, out EGender gender);

                User newUser = new User
                {
                    Name = TxtName.Text,
                    LastName = TxtLastName.Text,
                    UserName = TxtUserName.Text,
                    Password = TxtPassword.Text,
                    JMBG = TxtJMBG.Text,
                    Email = TxtEmail.Text,
                    Address = TxtAddress.Text,
                    Gender = gender,
                    UserType = EType.PACIJENT,
                    Active = true
                };

                Util.Instance.Users.Add(newUser);
                Util.Instance.addEntity(newUser);

                Patient newPatient = new Patient
                {
                    UserJMBG = newUser.JMBG
                };
                Util.Instance.Patients.Add(newPatient);

                this.Close();
            }
            else
            {
                MessageBox.Show(mistakeStr, "Greska");
            }
        }
    }
}
