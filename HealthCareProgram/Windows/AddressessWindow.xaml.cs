﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for AddressessWindow.xaml
    /// </summary>
    public partial class AddressessWindow : Window
    {
        ICollectionView view;
        User loggedUser;
        public AddressessWindow(User user)
        {
            loggedUser = user;
            InitializeComponent();
            Util.Instance.readEntities("adrese");
            UpdateView();
        }

        private void UpdateView()
        {
            if (loggedUser.UserType == EType.ADMINISTRATOR)
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.Addresses);
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.activeAddresses());
                MIAddress.IsEnabled = false;
            }

            DGAddresses.ItemsSource = view;
            DGAddresses.IsSynchronizedWithCurrentItem = true;
            
            DGAddresses.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void DGAddresses_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            
        }

        private void MIAddAddress_Click(object sender, RoutedEventArgs e)
        {
            Address address = new Address();
            AddEditAddress window = new AddEditAddress(address, EAddEdit.ADD);
            window.ShowDialog();
        }
        private void MIEditAddress_Click(object sender, RoutedEventArgs e)
        {
            Address selectedAddress = view.CurrentItem as Address;
            AddEditAddress window = new AddEditAddress(selectedAddress, EAddEdit.EDIT);
            window.ShowDialog();
        }

        private void MIDeleteAddress_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Address selectedAddress = view.CurrentItem as Address;
                Util.Instance.deleteEntity(selectedAddress);
            }
        }

        private void MIRestoreAddress_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Address selectedAddress = view.CurrentItem as Address;
                Util.Instance.restoreEntity(selectedAddress);
            }
        }
    }
}
