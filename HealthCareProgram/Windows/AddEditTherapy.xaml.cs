﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for AddEditTherapy.xaml
    /// </summary>
    public partial class AddEditTherapy : Window
    {
        private EAddEdit chosenOperation;
        private Therapy choosenTherapy;
        public AddEditTherapy(Therapy therapy, EAddEdit operation)
        {
            InitializeComponent();

            this.DataContext = therapy;

            choosenTherapy = therapy;
            chosenOperation = operation;

            if (operation.Equals(EAddEdit.ADD))
            {
                this.Title = "Dodavanje terapije";
            }
            else
            {
                this.Title = "Izmena terapije";

                TxtDescription.Text = therapy.Description;
                TxtDoctorJMBG.Text = therapy.DoctorJMBG;
                TxtPatientJMBG.Text = therapy.PatientJMBG;
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
        
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            string mistakeStr = "";
            bool mistakeBool = true;

            if (TxtDescription.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Opis" + "\n";
                mistakeBool = false;
            }
            if (TxtDoctorJMBG.Text.Equals("") || TxtDoctorJMBG.Text.Length != 13)
            {
                mistakeStr += "- Niste pravilno uneli JMBG doktora" + "\n";
                mistakeBool = false;
            }
            if (TxtPatientJMBG.Text.Equals("") || TxtPatientJMBG.Text.Length != 13)
            {
                mistakeStr += "- Niste pravilno uneli JMBG pacijenta" + "\n";
                mistakeBool = false;
            }

            if (mistakeBool)
            {
                if (chosenOperation.Equals(EAddEdit.ADD))
                {
                    Therapy newTherapy = new Therapy
                    {
                        ID = Util.Instance.getRandomID(),
                        DoctorJMBG = TxtDoctorJMBG.Text,
                        PatientJMBG = TxtPatientJMBG.Text,
                        Description = TxtDescription.Text,

                    };
                    Util.Instance.Therapies.Add(newTherapy);
                    Util.Instance.addEntity(newTherapy);
                }
                else
                {
                    Util.Instance.updateEntity(choosenTherapy);
                }

                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show(mistakeStr, "Greska");
            }
        }
    }
}
