﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for DoctorWindow.xaml
    /// </summary>
    public partial class DoctorWindow : Window
    {
        ICollectionView view;
        public DoctorWindow()
        {
            InitializeComponent();
            Util.Instance.readEntities("doktori");
            UpdateView();
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.Doctors);
            DGDoctors.ItemsSource = view;
            DGDoctors.IsSynchronizedWithCurrentItem = true;

            DGDoctors.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view.Filter = hccByAddressIdFilter;
        }

        private void DGDoctors_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

        }

        private bool hccByAddressIdFilter(object obj)
        {
            Doctor doctor = obj as Doctor;
            
            if (TxtFind.Text != "")
            {
                return doctor.HealthCareCentreID.Contains(TxtFind.Text);
            }
            else
                return true;

            return false;
        }

        private void TxtFind_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
