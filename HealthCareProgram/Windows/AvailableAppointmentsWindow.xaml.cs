﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{

    public partial class AvailableAppointmentsWindow : Window
    {
        ICollectionView view;
        User loggedUser = new User();
        ObservableCollection<Appointment> wantedAppointments;

        public AvailableAppointmentsWindow(User user)
        {
            loggedUser = user;

            InitializeComponent();

            UpdateView();
        }

        private void UpdateView()
        {
            view = CollectionViewSource.GetDefaultView(Util.Instance.findAvailableAppointments());

            DGAppointments.ItemsSource = view;
            DGAppointments.IsSynchronizedWithCurrentItem = true;
            DGAppointments.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            view.Filter = appointmentByHccIdFilter;
        }

        private void DGAppointment_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

        }

        private void MIAddAppointment_Click(object sender, RoutedEventArgs e)
        {
            Appointment selectedAppointment = view.CurrentItem as Appointment;

            if (selectedAppointment.Status == EStatus.SLOBODAN)
            {
                selectedAppointment.PatientJMBG = loggedUser.JMBG;
                selectedAppointment.Status = EStatus.ZAKAZAN;

                Util.Instance.updateEntity(selectedAppointment);
            }
            else
            {
                MessageBox.Show("Ovaj termin je vec zakazan", "Greska");
            }
        }

        private void MIRestoreAppointment_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Appointment selectedAppointment = view.CurrentItem as Appointment;
                Util.Instance.restoreEntity(selectedAppointment);
            }
        }

        private bool appointmentByHccIdFilter(object obj)
        {
            Appointment appointment = obj as Appointment;

            if (loggedUser.Active == true)
            {
                if (TxtFind.Text != "")
                {
                    return appointment.HealthCareCentreID.Contains(TxtFind.Text);
                }
                else
                    return true;
            }

            return false;
        }

        private void TxtFind_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void MIFindPatientTherapies_Click(object sender, RoutedEventArgs e)
        {
            Appointment selectedAppointment = view.CurrentItem as Appointment;

            if (selectedAppointment != null)
            {
                PatientTherapyWindow patientTherapyWindow = new PatientTherapyWindow(Util.Instance.findTherapies(selectedAppointment.PatientJMBG), loggedUser);
                patientTherapyWindow.ShowDialog();
            }
        }
    }
}
