﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HealthCareProgram.Models;

namespace HealthCareProgram.Windows
{
    public partial class AddEditAddress : Window
    {
        private EAddEdit chosenOperation;
        private Address chosenAddress;

        public AddEditAddress(Address address, EAddEdit operation)
        {
            InitializeComponent();

            this.DataContext = address;

            chosenAddress = address;
            chosenOperation = operation;

            if (operation.Equals(EAddEdit.ADD))
            {
                this.Title = "Dodavanje  adrese";
            }
            else
            {
                this.Title = "Izmena adrese";

                TxtStreetName.Text = chosenAddress.StreetName;
                TxtStreetNumber.Text = chosenAddress.StreetNumber;
                TxtCity.Text = chosenAddress.City;
                TxtCountry.Text = chosenAddress.Country;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            string mistakeStr = "";
            bool mistakeBool = true;

            if (TxtStreetName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Ime ulice" + "\n";
                mistakeBool = false;
            }
            if (TxtStreetNumber.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Broj ulice" + "\n";
                mistakeBool = false;
            }
            if (TxtCity.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Grad" + "\n";
                mistakeBool = false;
            }
            if (TxtCountry.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Drzavu" + "\n";
                mistakeBool = false;
            }

            if (mistakeBool)
            {
                if (chosenOperation.Equals(EAddEdit.ADD))
                {
                    Address newAddress = new Address
                    {
                        ID = Util.Instance.getRandomID(),
                        StreetName = TxtStreetName.Text,
                        StreetNumber = TxtStreetNumber.Text,
                        City = TxtCity.Text,
                        Country = TxtCountry.Text
                    };
                    Util.Instance.Addresses.Add(newAddress);
                    Util.Instance.addEntity(newAddress);
                } 
                else
                {
                    Util.Instance.updateEntity(chosenAddress);
                }

                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show(mistakeStr, "Greska");
            }
        }
    }
}
