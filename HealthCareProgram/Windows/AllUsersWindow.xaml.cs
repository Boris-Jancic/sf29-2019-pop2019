﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for AllUsersWindow.xaml
    /// </summary>
    public partial class AllUsersWindow : Window
    {
        ICollectionView view;
        User loggedUser = new User();

        public AllUsersWindow(User user)
        {
            loggedUser = user;
            InitializeComponent();
            Util.Instance.readEntities("korisnici");
            CmbType.ItemsSource = Enum.GetValues(typeof(EType)).Cast<EType>();
            UpdateView();
        }

        private void UpdateView()
        {

            if (loggedUser.UserType != EType.ADMINISTRATOR)
            {
                MIUser.IsEnabled = false;
                view = CollectionViewSource.GetDefaultView(Util.Instance.activeUsers());
            }
            else
            {
                view = CollectionViewSource.GetDefaultView(Util.Instance.Users);
            }
            
            DGUsers.ItemsSource = view;
            DGUsers.IsSynchronizedWithCurrentItem = true;
            
            view.Filter = userFilter;
            DGUsers.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void DGUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
        }

        private void MIAddUser_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            AddEditUser window = new AddEditUser(user, EAddEdit.ADD);
            window.ShowDialog();
        }

        private void MIEditUser_Click(object sender, RoutedEventArgs e)
        {
            User user = view.CurrentItem as User;
            AddEditUser window = new AddEditUser(user, EAddEdit.EDIT);
            window.ShowDialog();
        }

        private void MIDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                User selectedUser = view.CurrentItem as User;
                Util.Instance.deleteEntity(selectedUser);
            }
        }

        private void MIRestoreUser_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                User selectedUser = view.CurrentItem as User;
                Util.Instance.restoreEntity(selectedUser);
            }
        }

        private bool userFilter(object obj)
        {
            User user = obj as User;

            if (TxtFindName.Text != "")
            {
                return user.Name.Contains(TxtFindName.Text);
            }
            else if (TxtFindLastName.Text != "")
            {
                return user.LastName.Contains(TxtFindLastName.Text);
            }
            else if (TxtFindEmail.Text != "")
            {
                return user.Email.Contains(TxtFindEmail.Text);
            }
            else if (TxtFindAddress.Text != "")
            {
                return user.Address.Contains(TxtFindAddress.Text);
            }
            else if (CmbType.SelectedItem != null)
            {
                Enum.TryParse(CmbType.SelectedItem.ToString(), out EType type);
                return user.UserType.Equals(type);
            }

            return true;
        }

        private void TxtFindName_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindLastName_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindEmail_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindAddress_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void CmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            view.Refresh();
        }
    }
}
