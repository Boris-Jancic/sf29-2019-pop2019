﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for HealthCareCentreWindow.xaml
    /// </summary> 
    public partial class HealthCareCentreWindow : Window
    {
        ICollectionView view;
        User loggedUser = new User();
        public HealthCareCentreWindow(User user)
        {
            loggedUser = user;
            Util.Instance.readEntities("bolnice");
            InitializeComponent();
            UpdateView();
        }

        private void UpdateView()
        {
            if (loggedUser != null)
            {
                if (loggedUser.UserType == EType.ADMINISTRATOR)
                {
                    view = CollectionViewSource.GetDefaultView(Util.Instance.HealthCareCentres);
                    MIHealthCareCentre.IsEnabled = true;
                }
                else
                {
                    view = CollectionViewSource.GetDefaultView(Util.Instance.activeHealthCareCentres());
                }
            }

            DGHealthCareCentres.ItemsSource = view;
            DGHealthCareCentres.IsSynchronizedWithCurrentItem = true;

            DGHealthCareCentres.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view.Filter = hccByAddressIdFilter;
        }

        private void DGHealthCareCentres_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

        }

        private void MIAddHealthCareCentre_Click(object sender, RoutedEventArgs e)
        {
            HealthCareCentre healthCareCentre = new HealthCareCentre();
            AddEditHealthCareCentre window = new AddEditHealthCareCentre(healthCareCentre, EAddEdit.ADD);
            window.ShowDialog();
        }

        private void MIEditHealthCareCentre_Click(object sender, RoutedEventArgs e)
        {
            HealthCareCentre healthCareCentre = view.CurrentItem as HealthCareCentre;
            AddEditHealthCareCentre window = new AddEditHealthCareCentre(healthCareCentre, EAddEdit.EDIT);
            window.ShowDialog();
        }

        private void MIDeleteHealthCareCentre_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                HealthCareCentre selectedHealthCareCentre = view.CurrentItem as HealthCareCentre;
                Util.Instance.deleteEntity(selectedHealthCareCentre);
            }
        }

        private void MIrestoreHealthCareCentre_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                HealthCareCentre selectedHealthCareCentre = view.CurrentItem as HealthCareCentre;
                Util.Instance.restoreEntity(selectedHealthCareCentre);
            }
        }

        private bool hccByAddressIdFilter(object obj)
        {
            HealthCareCentre healthCareCentre = obj as HealthCareCentre;
            
            if (TxtFind.Text != "")
            {
                return healthCareCentre.AddressID.Contains(TxtFind.Text);
            }
            else
                return true;

        }

        private void TxtFind_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
