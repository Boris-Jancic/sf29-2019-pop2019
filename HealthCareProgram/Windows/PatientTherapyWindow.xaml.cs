﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for TherapyWindow.xaml
    /// </summary>
    public partial class PatientTherapyWindow : Window
    {
        public ICollectionView view;
        ObservableCollection<Therapy> patientTherapies;
        User loggedUser;

        public PatientTherapyWindow(ObservableCollection<Therapy> therapies, User user)
        {
            patientTherapies = therapies;
            loggedUser = user;

            Util.Instance.readEntities("terapije");
            InitializeComponent();
            UpdateView();
        }

        private void UpdateView()
        {
            if (loggedUser.UserType == EType.PACIJENT)
            {
                MITherapy.IsEnabled = false;
            }

            view = CollectionViewSource.GetDefaultView(patientTherapies);
            DGTherapies.ItemsSource = view;
            DGTherapies.IsSynchronizedWithCurrentItem = true;

            DGTherapies.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view.Filter = therapyFilter;
        }

        private void DGTherapies_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

        }

        private void MIAddTherapy_Click(object sender, RoutedEventArgs e)
        {
            Therapy therapy = new Therapy();
            AddEditTherapy window = new AddEditTherapy(therapy, EAddEdit.ADD);
            window.TxtDoctorJMBG.Text = loggedUser.JMBG;
            window.ShowDialog();
        }

        private void MIEditTherapy_Click(object sender, RoutedEventArgs e)
        {
            Therapy therapy = view.CurrentItem as Therapy;
            AddEditTherapy window = new AddEditTherapy(therapy, EAddEdit.EDIT);
            window.ShowDialog();
        }

        private void MIDeleteTherapy_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Therapy selectedTherapy = view.CurrentItem as Therapy;
                Util.Instance.deleteEntity(selectedTherapy);
            }
        }

        private void MIRestoreTherapy_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Therapy selectedTherapy = view.CurrentItem as Therapy;
                Util.Instance.restoreEntity(selectedTherapy);
            }
        }

        private bool therapyFilter(object obj)
        {
            Therapy therapy = obj as Therapy;

            if (TxtFindDescription.Text != "")
            {
                return therapy.Description.Contains(TxtFindDescription.Text);
            }
            else if (TxtFindDoctorJMBG.Text != "")
            {
                return therapy.DoctorJMBG.Contains(TxtFindDoctorJMBG.Text);
            }
            return true;
        }

        private void TxtFindDescription_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void TxtFindDoctorJMBG_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
