﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    public partial class AddEditUser : Window
    {
        private EAddEdit chosenOperation;
        private User choosenUser;

        public AddEditUser(User user, EAddEdit operation)
        {
            InitializeComponent();

            this.DataContext = user;

            choosenUser = user;
            chosenOperation = operation;

            CmbGender.ItemsSource = Enum.GetValues(typeof(EGender)).Cast<EGender>();
            CmbType.ItemsSource = Enum.GetValues(typeof(EType)).Cast<EType>();

            if (operation.Equals(EAddEdit.ADD))
            {
                this.Title = "Dodavanje korisnika";
            }
            else
            {
                this.Title = "Izmena korisnika";

                TxtName.Text = user.Name;
                TxtLastName.Text = user.LastName;
                TxtUserName.Text = user.UserName;
                TxtPassword.Text = user.Password;
                TxtJMBG.Text = user.JMBG;
                TxtEmail.Text = user.Email;
                TxtAddress.Text = user.Address;
                CmbGender.SelectedItem = user.Gender;
                CmbType.SelectedItem = user.UserType;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            string mistakeStr = "";
            bool mistakeBool = true;

            if (TxtName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Ime" + "\n";
                mistakeBool = false;
            }
            if (TxtLastName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Prezime" + "\n";
                mistakeBool = false;
            }
            if (TxtUserName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Korisnicko ime" + "\n";
                mistakeBool = false;
            }
            if (TxtPassword.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Sifru" + "\n";
                mistakeBool = false;
            }
            if (TxtJMBG.Text.Equals("") || TxtJMBG.Text.Length != 13)
            {
                mistakeStr += "- Niste pravilno uneli JMBG" + "\n";
                mistakeBool = false;
            }
            if (TxtEmail.Text.Equals("") || !TxtEmail.Text.Contains("@gmail.com"))
            {
                mistakeStr += "- Niste uneli Email" + "\n";
                mistakeBool = false;
            }
            if (TxtAddress.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli ID Adrese" + "\n";
                mistakeBool = false;
            }
            if (CmbGender.SelectedItem == null)
            {
                mistakeStr += "- Niste odabrali Pol korisnika" + "\n";
                mistakeBool = false;
            }
            if (CmbType.SelectedItem == null)
            {
                mistakeStr += "- Niste odabrali Tip korisnika" + "\n";
                mistakeBool = false;
            }

            if (mistakeBool) { 
                if (chosenOperation.Equals(EAddEdit.ADD))
                {
                    string genderValue = CmbGender.SelectedItem.ToString();
                    Enum.TryParse(genderValue, out EGender gender);

                    string typeValue = CmbType.SelectedItem.ToString();
                    Enum.TryParse(typeValue, out EType type);

                    User newUser = new User
                    {
                        Name = TxtName.Text,
                        LastName = TxtLastName.Text,
                        UserName = TxtUserName.Text,
                        Password = TxtPassword.Text,
                        JMBG = TxtJMBG.Text,
                        Email = TxtEmail.Text,
                        Address = TxtAddress.Text,
                        Gender = gender,
                        UserType = type,
                        Active = true
                    };

                    Util.Instance.Users.Add(newUser);
                    Util.Instance.addEntity(newUser);
                
                    if (newUser.UserType.Equals(EType.PACIJENT))
                    {
                        Patient newPatient = new Patient
                        {
                            UserJMBG = newUser.JMBG
                        };
                        Util.Instance.Patients.Add(newPatient);
                    }
                    else if (newUser.UserType.Equals(EType.LEKAR))
                    {
                        Doctor newDoctor = new Doctor
                        {
                            UserJMBG = newUser.JMBG
                        };
                        Util.Instance.Doctors.Add(newDoctor);
                    }
                    else if (newUser.UserType.Equals(EType.ADMINISTRATOR))
                    {
                        Admin newAdmin = new Admin
                        {
                            UserJMBG = newUser.JMBG
                        };
                        Util.Instance.Admins.Add(newAdmin);
                    }
                } 
                else if (chosenOperation.Equals(EAddEdit.EDIT))
                {
                    Util.Instance.updateEntity(choosenUser);
                }

                this.Close();
            }
            else
            {
                MessageBox.Show(mistakeStr, "Greska");
            }

        }
    }
}
