﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for AddEditHealthCareCentre.xaml
    /// </summary>
    public partial class AddEditHealthCareCentre : Window
    {
        private EAddEdit chosenOperation;
        private HealthCareCentre chosenHealthCareCentre;
        public AddEditHealthCareCentre(HealthCareCentre healthCareCentre, EAddEdit operation)
        {
            InitializeComponent();

            this.DataContext = healthCareCentre;

            chosenHealthCareCentre = healthCareCentre;
            chosenOperation = operation;

            if (operation.Equals(EAddEdit.ADD))
            {
                this.Title = "Dodavanje doma zdravlja";
            }
            else
            {
                this.Title = "Izmena doma zdravlja";

                TxtName.Text = chosenHealthCareCentre.Name;
                TxtAddressID.Text = chosenHealthCareCentre.AddressID;
            }


        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            string mistakeStr = "";
            bool mistakeBool = true;

            if (TxtName.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli Ime doma zdravlja" + "\n";
                mistakeBool = false;
            }
            if (TxtAddressID.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli ID Adrese" + "\n";
                mistakeBool = false;
            }

            if (mistakeBool)
            {
                if (chosenOperation.Equals(EAddEdit.ADD))
                {
                    HealthCareCentre newHealthCareCentre = new HealthCareCentre
                    {
                        ID = Util.Instance.getRandomID(),
                        Name = TxtName.Text,
                        AddressID = TxtAddressID.Text
                    };
                    Util.Instance.HealthCareCentres.Add(newHealthCareCentre);
                    Util.Instance.addEntity(newHealthCareCentre);
                } 
                else
                {
                    Util.Instance.updateEntity(chosenHealthCareCentre);
                }

                this.DialogResult = true;
                this.Close();
            }
            else
            {
                MessageBox.Show(mistakeStr, "Greska");
            }
        }

    }
}
