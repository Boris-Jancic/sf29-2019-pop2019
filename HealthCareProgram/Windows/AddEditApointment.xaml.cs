﻿using HealthCareProgram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HealthCareProgram.Windows
{
    /// <summary>
    /// Interaction logic for AddEditApointment.xaml
    /// </summary>
    public partial class AddEditApointment : Window
    {
        private EAddEdit chosenOperation;
        private Appointment choosenAppointment;

        public AddEditApointment(User doctor, Appointment appointment, EAddEdit operation)
        {
            InitializeComponent();

            this.DataContext = appointment;

            choosenAppointment = appointment;
            chosenOperation = operation;

            if (doctor != null)
            {
                if (doctor.UserType == EType.LEKAR)
                {
                    TxtDoctorJMBG.Text = doctor.JMBG;
                    TxtDoctorJMBG.IsEnabled = false;
                }
            }

            CmbStatus.ItemsSource = Enum.GetValues(typeof(EStatus)).Cast<EStatus>();

            if (operation.Equals(EAddEdit.ADD))
            {
                CmbStatus.IsEnabled = false;
                this.Title = "Dodavanje termina";
            }
            else
            {
                this.Title = "Izmena termina";

                TxtDoctorJMBG.Text = appointment.DoctorJMBG;
                TxtPatientJMBG.Text = appointment.PatientJMBG;
                datePicker.SelectedDate = appointment.Date;
                CmbStatus.SelectedItem = appointment.Status;
            }
        }


        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            string mistakeStr = "";
            bool mistakeBool = true;

            if (TxtDoctorJMBG.Text.Equals("") || TxtDoctorJMBG.Text.Length != 13)
            {
                mistakeStr += "- Niste pravilno uneli JMBG doktora" + "\n";
                mistakeBool = false;
            }
            if (TxtPatientJMBG.Text.Equals("") || TxtPatientJMBG.Text.Length != 13)
            {
                mistakeStr += "- Niste pravilno uneli JMBG pacijenta" + "\n";
                mistakeBool = false;
            }
            if (CmbStatus.SelectedItem == null)
            {
                mistakeStr += "- Niste izabrali stanje" + "\n";
                mistakeBool = false;
            }
            if (TxtHealthCareCentre.Text.Equals(""))
            {
                mistakeStr += "- Niste uneli ID doma zdravlja" + "\n";
                mistakeBool = false;
            }

            if (mistakeBool)
            {
                if (chosenOperation.Equals(EAddEdit.ADD))
                {
                    string statusValue = CmbStatus.SelectedItem.ToString();
                    Enum.TryParse(statusValue, out EStatus status);

                    Appointment newAppointment = new Appointment
                    {
                        ID = Util.Instance.getRandomID(),
                        DoctorJMBG = TxtDoctorJMBG.Text,
                        PatientJMBG = TxtPatientJMBG.Text,
                        Date = (DateTime) datePicker.SelectedDate,
                        Status = EStatus.SLOBODAN,
                        HealthCareCentreID = TxtHealthCareCentre.Text
                    };
                    Util.Instance.Appointments.Add(newAppointment);
                    Util.Instance.addEntity(newAppointment);
                }
                else
                {
                    Util.Instance.updateEntity(choosenAppointment);
                }

                this.Close();
            }
            else
            {
                MessageBox.Show(mistakeStr, "Greska");
            }
        }
    }
}
